# IANA data files

This is a mirror of the data files for protocols and services provided by IANA.

We mirror them here, because we need a reliable way of obtaining them.

The files are not versioned, but instead get overwritten on their servers as
they update them.

## Update the files

Run the script provided:

```
$ ./update-csv.sh
```

This should get the latest version of the data files from the IANA servers.
